document.addEventListener('DOMContentLoaded', () => {
  const addButton = document.getElementById('add-todo');
  const newTodoInput = document.getElementById('new-todo');
  const todoList = document.getElementById('todo-list');

  addButton.addEventListener('click', addTodo);

  function addTodo() {
    const todoText = newTodoInput.value.trim();
    if (todoText === '') return;

    const listItem = document.createElement('li');
    listItem.innerHTML = `
      <input type="checkbox" class="mr-2">
      <span>${todoText}</span>
      <button class="text-red-500 ml-2"><i class="fas fa-trash"></i></button>
    `;

    todoList.appendChild(listItem);
    newTodoInput.value = '';

    // Add event listeners for the checkbox and delete button
    listItem.querySelector('input[type="checkbox"]').addEventListener('change', toggleTodo);
    listItem.querySelector('button').addEventListener('click', deleteTodo);
  }

  function toggleTodo(event) {
    const todoItem = event.target.parentElement;
    todoItem.classList.toggle('line-through');
  }

  function deleteTodo(event) {
    const todoItem = event.target.closest('li');
    todoList.removeChild(todoItem);
  }
});
